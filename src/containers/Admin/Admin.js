import React, {useEffect, useState} from 'react';
import Grid from "@material-ui/core/Grid";
import makeStyles from "@material-ui/core/styles/makeStyles";
import MenuItem from "@material-ui/core/MenuItem";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import {CATEGORIES} from "../../constants";
import axiosContent from "../../axios-content";

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2)
    }
}));

const Admin = props => {
    const classes = useStyles();
    const name = props.match.params.name;

    const [content, setContent] = useState({
        category: '',
        title: '',
        content: ''
    });

    useEffect(() => {

        const fetchData = async () => {

            if(name) {
                const url = name + '.json';

                const response = await axiosContent.get(url);

                const pages = Object.keys(response.data).map(id => ({
                    ...response.data[id],
                    id
                }))

                console.log(pages)
            }
        };

        fetchData().catch(console.error);
    }, [name]);

    const editPage = async event => {
        event.preventDefault();

        const editContent = {
            category: content.category,
            title: content.title,
            content: content.content
        }

        try {
            await axiosContent.put('/pages/' + name + '.json', editContent);
        } finally {
            props.history.push('/');
        }
    };

    const changeContent = event => {
        const {name, value} = event.target;

        setContent(prev => ({
            ...prev,
            [name]: value
        }));
    };

    return (
        <Paper className={classes.paper}>
            <form onSubmit={editPage}>
                <Grid container direction="column" spacing={2}>
                    <Grid item xs>
                        <Typography variant="h5">Edit pages</Typography>
                    </Grid>
                    <Grid item xs>
                        <TextField
                            variant="outlined"
                            fullWidth
                            select
                            label="Category"
                            name="category"
                            value={content.category}
                            required
                            onChange={changeContent}
                        >
                            {CATEGORIES.map(c => (
                                <MenuItem key={c.id} value={c.id}>{c.title}</MenuItem>
                            ))}
                        </TextField>
                    </Grid>
                    <Grid item xs>
                        <TextField
                            variant="outlined"
                            fullWidth
                            type="text"
                            name="title"
                            label="Title"
                            value={content.title}
                            required
                            onChange={changeContent}
                        />
                    </Grid>
                    <Grid item xs>
                        <TextField
                            variant="outlined"
                            fullWidth
                            type="content"
                            name="content"
                            label="Content"
                            value={content.content}
                            required
                            onChange={changeContent}
                            multiline
                            rows={5}
                        />
                    </Grid>
                    <Grid item xs>
                        <Button type="submit" variant="contained" color="primary">Edit</Button>
                    </Grid>
                </Grid>
            </form>
        </Paper>
    );
};

export default Admin;