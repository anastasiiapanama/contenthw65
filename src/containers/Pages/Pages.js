import React, {useEffect, useState} from 'react';
import PaperWithPadding from "../../components/UI/PaperWithPadding";
import axiosContent from "../../axios-content";
import Grid from "@material-ui/core/Grid";

const Pages = ({match}) => {
    const [pages, setPages] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            let url = '/pages.json';

            if (match.params.name) {
                url += `?orderBy="category"&equalTo="${match.params.name}"`
            }

            const pagesResponse = await axiosContent.get(url);

            const pages = Object.keys(pagesResponse.data).map(id => ({
                ...pagesResponse.data[id],
                id
            }));
            console.log(pages)

            setPages(pages);
        };

        fetchData().catch(console.error);
    }, [match.params.name]);

    return (
        <Grid container direction="column" spacing={2}>
            {pages.map(page => (
                <Grid item xs key={page.id}>
                    <PaperWithPadding>
                        <h2>{page.title}</h2>
                        <p>{page.content}</p>
                    </PaperWithPadding>
                </Grid>
            ))}
        </Grid>
    );
};

export default Pages;