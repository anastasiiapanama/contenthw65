import React from 'react';
import NavigationItem from "./NavigationItem/NavigationItem/NavigationItem";
import './NavigationItems.css';
import {NavLink} from "react-router-dom";
import {CATEGORIES} from "../../../constants";

const NavigationItems = () => {
    return (
        <ul className="NavigationItems">
            <NavigationItem to="/" exact>Pages</NavigationItem>
            {CATEGORIES.map(c => (
                <NavigationItem key={c.id} component={NavLink} to={"/pages/" + c.id}>{c.title}</NavigationItem>
            ))}
            <NavigationItem to="/pages/admin" exact>Admin</NavigationItem>
        </ul>
    );
};

export default NavigationItems;